<?php

/**
 * @file
 * Provides the Keys API's administrative interface.
 */

/*******************************************************************************
 * Callback Functions, Forms, and Tables
 ******************************************************************************/

/**
 * Keys API List.
 */
function keys_ui_list() {
  $keys = keys_get_keys();

  return theme('keys_ui_list', array('keys' => $keys));
}

/**
 * Keys API add key form.
 */
function keys_ui_key_form($form, &$form_state, $key = NULL) {
  if (empty($key)) {
    $key = new stdClass();
    $key->kid = 0;
    $key->service = $key->rule = $key->api_key = '';
  }

  $form = keys_settings_form($key);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Save"),
  );
  $form['#submit'][] = 'keys_ui_key_form_submit';
  $form['#redirect'] = 'admin/config/keys';

  return $form;
}

function keys_ui_key_form_submit($form, &$form_state) {
  $message = t('Key successfully saved.');
  drupal_set_message($message);
}

function keys_ui_key_delete_form($form, &$form_state, $key) {
  $form['kid'] = array(
    '#type' => 'value',
    '#value' => $key->kid
  );

  return confirm_form($form,
    t("Are you sure you want to delete key for '%service' @ '%rule'?", array('%service' => $key->service, '%rule' => $key->rule)),
    'admin/config/keys/settings',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function keys_ui_key_delete_form_submit($form, &$form_state) {
  $deleted = db_delete('keys_api')
    ->condition('kid', $form_state['values']['kid'])
    ->execute();
  drupal_set_message(t('Key deleted successfully.'));
  $form_state['redirect'] = 'admin/config/keys/settings';
}

/*******************************************************************************
 * Module and Helper Functions
 ******************************************************************************/

function keys_ui_delete_key() {
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    db_query("DELETE FROM {keys_api} WHERE rule = '%s' AND service = '%s'", $_POST['name'], $_POST['service']);
    $deleted = db_delete('keys_api')
      ->condition('rule', $_POST['name'])
      ->condition('service', $_POST['service'])
      ->execute();
    print keys_ui_get_keys();
  }
  exit();
}

/*******************************************************************************
 * Theme Functions
 ******************************************************************************/

function theme_keys_ui_list($vars) {
  $keys = $vars['keys'];

  if (empty($keys)) {
    return t("No keys have been saved.");
  }

  $list = keys_get_services();

  $headers = array(
    array('data' => t('Service')),
    array('data' => t('Rule')),
    array('data' => t('API Key')),
    array('data' => '')
  );
  $rows = array();

  $links = array(
    'edit' => array('title' => 'Edit'),
    'delete' => array('title' => 'Delete')
  );

  foreach ($keys as $key) {
    $links['edit']['href'] = 'admin/config/keys/'. $key->kid .'/edit';
    $links['delete']['href'] = 'admin/config/keys/'. $key->kid .'/delete';
    $rows[] = array(
      array('data' => $list[$key->service]['name']),
      array('data' => $key->rule),
      array('data' => $key->api_key),
      array('data' => theme('links', array('links' => $links)))
    );
  }

  return theme('table', array('header' => $headers, 'rows' => $rows));
}

function theme_keys_ui_key_form($vars) {
  $form = $vars['form'];
  drupal_add_js(array('keys' => array('domain' => keys_get_domain())), 'setting');
  drupal_add_js(drupal_get_path('module', 'keys_ui') .'/keys_ui.js');
  return drupal_render($form);
}
